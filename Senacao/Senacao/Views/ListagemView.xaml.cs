﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacao.Views
{
    public class Servico
    {
        public string Nome { get; set; }
        public float Preco { get; set; }
        public float PrecoOpcionais { get; set; }
        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco);  } 
        }
    }

    public partial class ListagemView : ContentPage
    {
        public ObservableCollection<Servico> Servicos { get; set;  } 

        public ListagemView()
        {
            InitializeComponent();
            this.Servicos = new ObservableCollection<Servico>();
            //this.Servicos = new List<Servico>
            //{
            //    new Servico { Nome = "Banho e Tosa", Preco = 80 },
            //    new Servico { Nome = "Tosa Higiênica", Preco = 60 },
            //    new Servico { Nome = "Consulta Veterinária", Preco = 150},
            //    new Servico { Nome = "Retorno Consulta", Preco = 50 },
            //    new Servico { Nome = "Exames", Preco = 120 }
            //};
            ListViewServicos.RefreshCommand = new Command(() =>
            {
                InitServiceList();
                ListViewServicos.IsRefreshing = false;
            });
            this.BindingContext = this;

            InitServiceList();
        }

        private void ListViewServicos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;

            Navigation.PushAsync( new DescricaoView(servico) );

            //DisplayAlert("Serviço", string.Format("" +
            //    "Você selecionou o serviço '{0}'. Valor {1}",
            //    servico.Nome, servico.PrecoFormatado), "OK");

        }

        private void ButtonAgendamentos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync( new ListagemAgendamentosView() );
        }

        public async void InitServiceList()
        {
            Console.WriteLine("IniciandoConsumo de API");
            Uri url = new Uri("http://10.141.46.15:3003/servicos");

            // CALL ENDPOINT
            HttpResponseMessage httpResponse =
             await Services.HttpServices.GetRequest(url.AbsoluteUri);
            if(httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n=========================");
                Console.WriteLine(stringResponse);

                List<Servico> ListServicos =
                    Services.SerializationService
                    .DeserializeArrayObject<Servico>(stringResponse);

                this.Servicos.Clear();
                foreach(Servico servico in ListServicos)
                {
                    this.Servicos.Add(servico);
                }

            }
        }
    }
}
